package Server;

import java.io.*;
import java.net.*;
import java.util.*;

/*
server端
 */


public class ChatServer {
    private ServerSocket ss;
    private boolean flag = false;
    private Map<String, Client> clients = new HashMap<>();

    public static void main(String args[]) {
        new ChatServer().start();
    }

    public void start() {
        try {
            ss = new ServerSocket(8889);
            flag = true;
            System.out.println("Server启动成功...");
        } catch (BindException e) {
            System.out.println("端口使用中!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //接收客户端的连接
            while (flag) {
                //accept方法是一个阻塞的方法，会阻塞当前的线程。
                Socket socket = ss.accept();
                Client c = new Client(socket);
                System.out.println("一个客户端已经连接...");
                new Thread(c).start();
            }
        } catch (IOException e) {
            System.out.println("Client closed...");
        } finally {
            try {
                if (ss != null)
                    ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    private class Client implements Runnable {
        private Socket s;
        private String userName = "";
        private DataInputStream input = null;
        private DataOutputStream output = null;
        private boolean connected = false;
        private BufferedOutputStream fout = null;
        private String saveFilePath = "";
        public Client(Socket s) {
            this.s = s;
            try {
                input = new DataInputStream(s.getInputStream());
                output = new DataOutputStream(s.getOutputStream());
                connected = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        @Override
        public void run() {
            byte buffer[] = new byte[1024];
            int len = 0;
            try {
                while (connected) {
                    String msg[] = input.readUTF().split("#");//msg={"  ","登录用户","目标用户","要发送的信息"}
                    switch (msg[0]) {
                        case "LOGIN":
                            String userName = msg[1];
                            if (clients.containsKey(userName)) {
                                output.writeUTF("FAIL");//已经登录
                                System.out.println("拒绝了一个重复连接...");
                                closeConnect();
                            } else {
                                output.writeUTF("SUCCESS");
                                clients.put(userName, this);
                                //将所有登录用户信息发送给新登录的用户
                                StringBuffer allUsers = new StringBuffer();
                                allUsers.append("ALLUSERS#");
                                for (String user : clients.keySet())
                                    allUsers.append(user + "#");
                                output.writeUTF(allUsers.toString());
                                //将新登录的用户信息发送给其他用户
                                String newLogin = "LOGIN#" + userName;
                                sendMsg(userName, newLogin);
                                this.userName = userName;
                            }
                            break;
                        case "LOGOUT":
                            clients.remove(this.userName);
                            String logoutMsg = "LOGOUT#" + this.userName;
                            sendMsg(this.userName, logoutMsg);
                            System.out.println("用户" + this.userName + "已下线...");
                            closeConnect();
                            break;
                        case "SENDONE":
                            Client c = clients.get(msg[1]);//获取目标用户的连接
                            String msgToOne="";
                            if (c != null) {
//                                if (("SENDFILE").equals(msg[2])) {
//                                    saveFilePath = "D:\\Chat\\upload\\" + msg[3];
//                                    fout = new BufferedOutputStream(new FileOutputStream(saveFilePath));
//                                    while ((len=input.read(buffer)) != -1) {
//                                        fout.write(buffer);
//                                        if(len<1024) {
//                                            fout.flush();
//                                            fout.close();
//                                            break;
//                                        }
//                                    }
//                                    msgToOne="SENDONE#" + this.userName + "#SENDFILE#" + msg[4]+"#"+saveFilePath;
//                                }else{//没有发送文件
                                    msgToOne="SENDONE#" + this.userName + "#" + msg[2];
//                                }
//                                String sendOne = "SENDONE#" + this.userName + "#" + msg[2];
                                c.output.writeUTF(msgToOne);
                                c.output.flush();
                            }
                            break;
                        case "SENDALL":
                            String msgToAll = "";

                            //如果客户端传输文件
//                            if (("SENDFILE").equals(msg[1])) {
//                                saveFilePath = "D:\\Chat\\upload\\" + msg[2];
//                                fout = new BufferedOutputStream(new FileOutputStream(saveFilePath));
//                                while ((len=input.read(buffer)) != -1) {
//                                    fout.write(buffer,0,len);
//                                    if(len<1024) {
//                                        fout.flush();
//                                        fout.close();
//                                        break;
//                                    }
//                                }
//                                msgToAll = "SENDALL#" + this.userName + "#SENDFILE#" + msg[3] + "#" + saveFilePath;
//                            } else
                                msgToAll = "SENDALL#" + this.userName + "#" + msg[1];
                            sendMsg(this.userName, msgToAll);
                            break;

                    }

                }
            } catch (IOException e) {
                System.out.println("Client closed...");
                connected = false;
            } finally {

                try {
                    if (input != null)
                        input.close();
                    if (output != null)
                        output.close();
                    if (fout != null)
                        fout.close();
                    if (s != null)
                        s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        public void closeConnect() {
            connected = false;
            try {
                if (input != null)
                    input.close();
                if (output != null)
                    output.close();
                if (s != null)
                    s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void sendMsg(String fromUser, String msg) {
            String tempUser = "";
            try {
                for (String toUser : clients.keySet()) {
                    if (!toUser.equals(fromUser)) {
                        tempUser = toUser;
                        DataOutputStream out = clients.get(toUser).output;
                        out.writeUTF(msg);
                        out.flush();
                    }
                }
            } catch (IOException e) {
                System.out.println("用户" + tempUser + "已经离线！！！");
            }
        }
    }
}
